//
//  SearchTableViewCell.m
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/6/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell

@synthesize movieImageView, movieLabel, lineLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)configureCellWithJSON:(NSDictionary *)jsonObj {
    self.lineLabel.text = [NSString stringWithFormat:@"%@", [jsonObj objectForKey:@"line"]];
    self.movieLabel.text = [NSString stringWithFormat:@"%@", [jsonObj objectForKey:@"title"]];
    
    // Show movie cover photo
    NSString *coverFile = [jsonObj objectForKey:@"coverPhoto"];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(loadImage:)
                                        object:coverFile];
    [queue addOperation:operation];
     
}

- (void)loadImage:(NSString *)file {
    NSString *urlString = [NSString stringWithFormat:@"http://quotacle.com/posters/%@.jpg", file];
    NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlString]];
    UIImage* image = [[UIImage alloc] initWithData:imageData];
    [self performSelectorOnMainThread:@selector(displayImage:) withObject:image waitUntilDone:NO];
}

- (void)displayImage:(UIImage *)image {
    [self.movieImageView setImage:image];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
