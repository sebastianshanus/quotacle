//
//  MasterViewController.h
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/6/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController <NSURLConnectionDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

