//
//  FeaturedTableViewCell.h
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/7/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeaturedTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel      *quoteLabel;
@property (strong, nonatomic) IBOutlet UILabel      *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView  *thumbnailImageView;

- (void)configureCellWithJSON:(NSDictionary *)jsonObj;

@end
