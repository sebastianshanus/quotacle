//
//  FeaturedTableViewCell.m
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/7/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import "FeaturedTableViewCell.h"

@implementation FeaturedTableViewCell

@synthesize quoteLabel, titleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)configureCellWithJSON:(NSDictionary *)jsonObj {
    self.titleLabel.text = [jsonObj objectForKey:@"title"];
    self.quoteLabel.text = [jsonObj objectForKey:@"line"];
    
    // Grab thumbnail
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://qrkj-sqvt.accessdomain.com/test/thumbnail.php?cleanfile=%@&time=%@", [jsonObj objectForKey:@"cleanFile"], [jsonObj objectForKey:@"time"]]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSData *jsonData = [result dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        
        if ([jsonObj objectForKey:@"success"]) {
            NSString *thumbnailURL = [jsonObj objectForKey:@"url"];
            NSOperationQueue *queue = [NSOperationQueue new];
            NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                                initWithTarget:self
                                                selector:@selector(loadImage:)
                                                object:thumbnailURL];
            [queue addOperation:operation];
        }
    }];
}

- (void)loadImage:(NSString *)url {
    NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
    UIImage* image = [[UIImage alloc] initWithData:imageData];
    [self performSelectorOnMainThread:@selector(displayImage:) withObject:image waitUntilDone:NO];
}

- (void)displayImage:(UIImage *)image {
    [self.thumbnailImageView setImage:image];
    [UIView beginAnimations:NULL context:nil];
    [UIView setAnimationDuration:1.0];
    self.thumbnailImageView.alpha = 1.0;
    [UIView commitAnimations];
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
