//
//  DetailViewController.h
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/6/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MediaPlayer/MediaPlayer.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSDictionary                      *detailItem;
@property (weak, nonatomic) IBOutlet UILabel                    *quoteLabel;
@property (weak, nonatomic) IBOutlet UIScrollView               *infoScrollView;
@property (weak, nonatomic) IBOutlet UIButton                   *replayButton;
@property (weak, nonatomic) IBOutlet UIImageView                *coverPhotoView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView    *spinnerView;
@property (weak, nonatomic) IBOutlet UIImageView                *videoLayoutView;
@property (weak, nonatomic) IBOutlet UILabel                    *moreQuotesLabel;
@property MPMoviePlayerController                               *moviePlayer;
@property NSURL                                                 *movieURL;
@property NSString                                              *moveTitle;
@property bool                                                  hasBegunLoading;
@property BOOL                                                  videoPlaying;

- (IBAction)pressedReplay:(id)sender;

@end

