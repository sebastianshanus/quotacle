//
//  SearchTableViewCell.h
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/6/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView  *movieImageView;
@property (strong, nonatomic) IBOutlet UILabel      *lineLabel;
@property (strong, nonatomic) IBOutlet UILabel      *movieLabel;

- (void)configureCellWithJSON:(NSDictionary *)jsonObj;

@end
