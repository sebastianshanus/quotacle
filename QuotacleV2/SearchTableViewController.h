//
//  SearchTableViewController.h
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/6/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UISearchBar  *lineSearchBar;
@property (strong, nonatomic) NSMutableArray        *searchResults;
@property (strong, nonatomic) NSString              *customSearchString;

- (void)searchFor:(NSString *)searchString;

@end
