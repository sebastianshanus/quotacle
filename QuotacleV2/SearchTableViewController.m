//
//  SearchTableViewController.m
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/6/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import "SearchTableViewController.h"

#import "DetailViewController.h"
#import "SearchTableViewCell.h"

#import "Flurry.h"

@interface SearchTableViewController () <UISearchBarDelegate, NSURLConnectionDelegate>


@end

@implementation SearchTableViewController

@synthesize lineSearchBar, searchResults;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:147.0/255.0 green:32.0/255.0 blue:37.0/255.0 alpha:0.9];
    
    searchResults = [[NSMutableArray alloc] init];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:147.0/255.0 green:32.0/255.0 blue:37.0/255.0 alpha:0.9]];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.title = @"Search";
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [titleLabel setText:@"Search"];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:20.0f]];
    [titleLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    self.navigationItem.titleView = titleLabel;
    [titleLabel sizeToFit];
}

- (void)viewDidAppear:(BOOL)animated {
    if (self.customSearchString) {
        [Flurry logEvent:@"Viewing more quotes from movie" withParameters:[NSDictionary dictionaryWithObject:self.customSearchString forKey:@"movie"]];
        [lineSearchBar setText:[NSString stringWithFormat:@"%@", self.customSearchString]];
        [self searchFor:self.customSearchString];
        self.customSearchString = nil;
    } else if ([[lineSearchBar text] isEqualToString:@""]) {
        [lineSearchBar becomeFirstResponder];
    }
}



- (void)searchFor:(NSString *)searchString {
    [Flurry logEvent:@"Searched for Quote" withParameters:[NSDictionary dictionaryWithObject:searchResults forKey:@"searchString"]];
    [searchResults removeAllObjects];
    [self.tableView reloadData];
    NSString *encodedString = [searchString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://qrkj-sqvt.accessdomain.com/test/index.php?search=%@&random=%d", encodedString, arc4random() % 99999]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if (![result isEqualToString:@"No results"] && ![searchString isEqualToString:@""]) {
            NSArray *objects = [result componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
            NSMutableArray *tempObjects = [NSMutableArray array];
            for (NSString *json in objects) {
                if (![json isEqualToString:@""]) {
                    NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
                    [tempObjects addObject:jsonObj];
                }
            }
            searchResults = tempObjects;
        }
        [self.tableView reloadData];
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UISearchBar delegate methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self searchFor:searchText];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return searchResults.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchTableViewCell *cell = (SearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SearchCell" forIndexPath:indexPath];
    
    // Configure the cell...
    [cell configureCellWithJSON:[searchResults objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [lineSearchBar resignFirstResponder];
}



#pragma mark - Orientation
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}




#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *jsonObject = self.searchResults[indexPath.row];
        DetailViewController *controller = [segue destinationViewController];
        [controller setDetailItem:jsonObject];
        [Flurry logEvent:@"Viewed searched quote" withParameters:[NSDictionary dictionaryWithObject:jsonObject forKey:@"json"]];
    }
}


@end
