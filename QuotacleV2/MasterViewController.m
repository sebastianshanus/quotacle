//
//  MasterViewController.m
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/6/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

#import "MBProgressHUD.h"

#import "FeaturedTableViewCell.h"

#import "Flurry.h"


@interface MasterViewController ()

@property NSMutableArray    *objects;
@property NSMutableData     *responseData;

- (void)parsePopularQuotes;

@end

@implementation MasterViewController
            
- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Quotacle";
    UIImageView *titleImage = [[UIImageView alloc] initWithFrame:CGRectZero];
    [titleImage setImage:[UIImage imageNamed:@"bigLogoWhite.png"]];
    self.navigationItem.titleView = titleImage;
    [titleImage sizeToFit];

    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.responseData = [[NSMutableData alloc] init];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:147.0/255.0 green:32.0/255.0 blue:37.0/255.0 alpha:0.9]];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self parsePopularQuotes];
}


- (void)parsePopularQuotes {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://qrkj-sqvt.accessdomain.com/test/popularquotes.php"]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
    [[NSURLCache sharedURLCache] removeCachedResponseForRequest:request];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if (connectionError) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bad Connection" message:@"Uh Oh. It looks like you don't have any reception. Please try again." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        } else if (![result isEqualToString:@"No results"]) {
            NSArray *objects = [result componentsSeparatedByString:@"\n"];
            NSMutableArray *tempObjects = [NSMutableArray array];
            for (NSString *json in objects) {
                if (![json isEqualToString:@""]) {
                    NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
                    
                    if (![[jsonObj objectForKey:@"title"] isEqualToString:@""]) {
                        [tempObjects addObject:jsonObj];
                    }
                }
            }
            self.objects = tempObjects;
        }
        [self.tableView reloadData];
    }];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *object = self.objects[indexPath.section];
        DetailViewController *controller = [segue destinationViewController];
        [controller setDetailItem:object];
        
        [Flurry logEvent:@"Viewed featured quote" withParameters:[NSDictionary dictionaryWithObject:object forKey:@"json"]];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.objects.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Most Popular Quotes";
    } else {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeaturedTableViewCell *cell = (FeaturedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FeaturedCell" forIndexPath:indexPath];

    NSDictionary *object = self.objects[indexPath.section];
    [cell configureCellWithJSON:object];
    return cell;
}

@end
