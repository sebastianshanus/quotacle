//
//  UINavigationController+Orientation.h
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/9/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Orientation)

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation;
-(BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;

@end