//
//  DetailViewController.m
//  QuotacleV2
//
//  Created by Shanus, Sebastian on 8/6/14.
//  Copyright (c) 2014 Ziga. All rights reserved.
//

#import "DetailViewController.h"

#import "SearchTableViewController.h"

@interface DetailViewController ()


@end

@implementation DetailViewController
            
#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        //[self configureView];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:147.0/255.0 green:32.0/255.0 blue:37.0/255.0 alpha:0.9]];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.replayButton setHidden:YES];
    
    [self configureView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.moviePlayer stop];
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.hasBegunLoading = false;
        self.videoPlaying = false;
        
        self.moveTitle = [self.detailItem objectForKey:@"title"];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [titleLabel setText:self.moveTitle];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setFont:[UIFont boldSystemFontOfSize:20.0f]];
        [titleLabel setShadowColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        self.navigationItem.titleView = titleLabel;
        [titleLabel sizeToFit];
        
        self.moreQuotesLabel.text = [NSString stringWithFormat:@"More from %@", self.moveTitle];
        
        self.quoteLabel.text = [self.detailItem objectForKey:@"line"];
        
        NSString *coverFile;
        if ([self.detailItem objectForKey:@"cleanFile"]) {
            coverFile = [self.detailItem objectForKey:@"cleanFile"];
        } else if ([self.detailItem objectForKey:@"coverPhoto"]) {
            coverFile = [self.detailItem objectForKey:@"coverPhoto"];
        }
        
        /*
        // Setup size of scroll
        //[self.infoScrollView setContentSize:CGSizeMake(320, 320)];
        
        // Configure view in scroll view
        UIImageView *whiteBackView = [[UIImageView alloc] initWithFrame:CGRectMake(14, 124, 288, 190)];
        [whiteBackView setImage:[UIImage imageNamed:@"whiteback.jpg"]];
        [self.infoScrollView addSubview:whiteBackView];
        
        //self.coverPhotoView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 129, 125, 180)];
        //[self.infoScrollView addSubview:self.coverPhotoView];
         */
        
        NSOperationQueue *queue = [NSOperationQueue new];
        NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                            initWithTarget:self
                                            selector:@selector(loadImage:)
                                            object:coverFile];
        [queue addOperation:operation];
        
        // Grab video url
        NSString *cleanfile;
        NSString *pureTime;
        if ([self.detailItem objectForKey:@"coverPhoto"]) {
            cleanfile = [self.detailItem objectForKey:@"coverPhoto"];
            pureTime = [self.detailItem objectForKey:@"pureTime"];
        } else if ([self.detailItem objectForKey:@"cleanFile"]) {
            cleanfile = [self.detailItem objectForKey:@"cleanFile"];
            pureTime = [self.detailItem objectForKey:@"time"];
        }
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://qrkj-sqvt.accessdomain.com/test/cutvideo.php?cleanfile=%@&time=%@", cleanfile, pureTime]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSData *jsonData = [result dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
            
            if ([jsonObj objectForKey:@"success"]) {
                self.movieURL = [NSURL URLWithString:[jsonObj objectForKey:@"url"]];
                [self setupVideo:self.movieURL];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Uh oh. Looks like there was an error of some kind. Please try again." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

- (void)setupVideo:(NSURL *)videoURL {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playBackStateChanged) name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishedPlayingVideo) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    self.moviePlayer = [[MPMoviePlayerController alloc] init];
    self.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
    [self.moviePlayer setContentURL:videoURL];
    [self.moviePlayer setControlStyle:MPMovieControlStyleNone];
    [self.moviePlayer.view setFrame:CGRectMake(0, 64, self.videoLayoutView.frame.size.width, self.videoLayoutView.frame.size.height)];
    [self.view addSubview:self.moviePlayer.view];
    [self.moviePlayer play];
    
    // Handle loading view
    [self.view bringSubviewToFront:self.spinnerView];
    [self.spinnerView startAnimating];
    
    [self performSelector:@selector(checkLoading) withObject:nil afterDelay:5];
}

- (void)loadImage:(NSString *)file {
    NSString *urlString = [NSString stringWithFormat:@"http://quotacle.com/posters/%@.jpg", file];
    NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlString]];
    UIImage* image = [[UIImage alloc] initWithData:imageData];
    [self performSelectorOnMainThread:@selector(displayImage:) withObject:image waitUntilDone:NO];
}

- (void)displayImage:(UIImage *)image {
    [self.coverPhotoView setImage:image];
}

- (IBAction)pressedReplay:(id)sender {
    [self.replayButton setHidden:YES];
    [self.moviePlayer play];
}

#pragma mark - Interface touching

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self.view];
    
    if (CGRectContainsPoint(self.moviePlayer.view.frame, location)) {
        if (self.hasBegunLoading && self.replayButton.hidden == true) {
            if (self.videoPlaying) {
                [self.moviePlayer pause];
                self.videoPlaying = false;
            } else {
                [self.moviePlayer play];
                self.videoPlaying = true;
            }
        }
    }
}


#pragma mark - Orientation
-(BOOL)shouldAutorotate
{
    if ([[UIDevice currentDevice] orientation] > 1) {
        int width = [[UIScreen mainScreen] bounds].size.width;
        int height = [[UIScreen mainScreen] bounds].size.height;
        if (height > width) {
            [self.moviePlayer.view setFrame:CGRectMake(0, 0, height, width)];
        } else {
            [self.moviePlayer.view setFrame:CGRectMake(0, 0, width, height)];
        }
        [[self navigationController] setNavigationBarHidden:YES animated:NO];
    } else {
        [self.moviePlayer.view setFrame:CGRectMake(0, 64, self.videoLayoutView.frame.size.width, self.videoLayoutView.frame.size.height)];
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
    }
    return YES;
}



#pragma mark - MPMoviePlayerController method

- (void)playBackStateChanged {
    [self.spinnerView stopAnimating];
    self.hasBegunLoading = true;
    
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
        self.videoPlaying = true;
        NSLog(@"true");
    } else {
        self.videoPlaying = false;
        NSLog(@"false");
    }
}

- (void)checkLoading {
    if (!self.hasBegunLoading) {
        [self setupVideo:self.movieURL];
    }
}

- (void)finishedPlayingVideo {
    if (self.hasBegunLoading) {
        [self.replayButton setHidden:NO];
        [self.view bringSubviewToFront:self.replayButton];
    }
}

#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showCustomSearch"]) {
        SearchTableViewController *searchViewController = [segue destinationViewController];
        [searchViewController setCustomSearchString:self.moveTitle];
    }
}


@end
